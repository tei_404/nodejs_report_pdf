const express =require('express');
const path =require('path');
const app =express();

app.use(express.json());
app.use('/js', express.static(path.join(__dirname, './js')))
app.use('/img', express.static(path.join(__dirname, './img')))

app.get('/', async (req, res) => {
    res.sendFile(path.join(__dirname, 'main.html'));
})
app.get('/DALY', async (req, res) => {
    res.sendFile(path.join(__dirname, 'daly.html'));
})
app.get('/SHORT', async (req, res) => {
    res.sendFile(path.join(__dirname, 'short.html'));
})
const port = 3000;
app.listen(port,()=>{console.log("Server Running at localhost:"+port)});